FROM node:18.12.0-alpine

WORKDIR /app

ADD . .

RUN npm install

ENTRYPOINT npm rebuild esbuild && npm start
