import express from "express";
import rtspRelay from "rtsp-relay";
import type { Application } from "express-ws";

const app = express() as unknown as Application;

const { proxy } = rtspRelay(app);

app.ws("/api/stream", proxy({ url: "rtsp://localhost:8331/stream" }));

app.listen(2000);
