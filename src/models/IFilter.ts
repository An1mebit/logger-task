export interface IFilter {
  level?: string;
  time?: {
    from?: string;
    to?: string;
  };
}
