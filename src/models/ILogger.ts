export interface ILogger {
  timestamp: string;
  level: string;
  message: string;
}
