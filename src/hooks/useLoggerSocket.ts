import { useContext } from "react";
import { LoggerSocketContext } from "../components/LoggerStorageProvider/LoggerStorageProvider";

const useLoggerSocket = (): WebSocket => {
  const socket = useContext(LoggerSocketContext);

  return socket;
};

export default useLoggerSocket;
