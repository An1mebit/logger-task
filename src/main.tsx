import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.scss";
import { BrowserRouter } from "react-router-dom";
import LoggerStorageProvider from "./components/LoggerStorageProvider/LoggerStorageProvider.tsx";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <BrowserRouter>
      <LoggerStorageProvider>
        <App />
      </LoggerStorageProvider>
    </BrowserRouter>
  </React.StrictMode>
);
