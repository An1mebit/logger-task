import dayjs from "dayjs";
import { ILogger } from "../../../models/ILogger";
import "../terminal.scss";
import { useEffect, useRef, useState } from "react";

const TerminalContainer = ({ logs }: { logs: ILogger[] }) => {
  const teriminalRef = useRef<HTMLDivElement | null>(null);
  const [waypoint, setWaypoint] = useState<boolean>(false);

  useEffect(() => {
    if (!waypoint && typeof teriminalRef.current?.scrollTo === "function") {
      teriminalRef.current.scrollTo({
        top: teriminalRef.current?.scrollHeight,
        behavior: "smooth",
      });
      setWaypoint(true);
    }
  }, [waypoint]);

  return (
    <div className="terminal">
      <div className="terminal_container" ref={teriminalRef}>
        {logs.map((item, index) => (
          <div key={index} className="terminal_item">
            <p>{`[${item.level}]`}</p>
            <p>{item.message}</p>
            <p>{dayjs(item.timestamp).format("DD.MM.YYYY")}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default TerminalContainer;
