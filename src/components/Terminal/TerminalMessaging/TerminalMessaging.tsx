import { Dispatch, FC, SetStateAction, useState } from "react";
import useLoggerSocket from "../../../hooks/useLoggerSocket";

interface ITerminalMessagingProps {
  setIsToast: Dispatch<SetStateAction<boolean>>;
}

const TerminalMessaging: FC<ITerminalMessagingProps> = ({ setIsToast }) => {
  const socket = useLoggerSocket();
  const [message, setMessage] = useState<string>("");

  const handlerChangeMessage = (value: string) => {
    setMessage(value);
  };

  const handlerSendClick = () => {
    socket.send(message);
    setMessage("");
    setIsToast(true);
  };

  const handlerClearTerminal = () => {
    localStorage.setItem("logs", "[]");
  };

  return (
    <div style={{ display: "flex", gap: "20px", justifyContent: "center" }}>
      <input
        type="text"
        value={message}
        onChange={(e) => handlerChangeMessage(e.target.value)}
      />
      <button onClick={handlerSendClick}>Отправить</button>
      <button onClick={handlerClearTerminal}>Очистить терминал</button>
    </div>
  );
};

export default TerminalMessaging;
