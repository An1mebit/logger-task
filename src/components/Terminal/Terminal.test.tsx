import { act, render, screen } from "@testing-library/react";
import { afterAll, beforeEach, describe, expect, it } from "vitest";
import { Terminal } from "./Terminal";
import LoggerStorageProvider from "../LoggerStorageProvider/LoggerStorageProvider";
import userEvent from "@testing-library/user-event";
import { ILogger } from "../../models/ILogger";

const saveLogs = () => {
  const logs: ILogger[] = JSON.parse(localStorage.getItem("logs") || "[]");
  logs.push({
    timestamp: "2023-03-03T08:15:30Z",
    level: "info",
    message: "Application started",
  });
  localStorage.setItem("logs", JSON.stringify(logs));
};

describe("Terminal", () => {
  beforeEach(() => {
    localStorage.setItem("logs", "[]");
    saveLogs();
    act(() => {
      render(
        <LoggerStorageProvider>
          <Terminal />
        </LoggerStorageProvider>
      );
    });
  });

  afterAll(() => {
    localStorage.clear();
  });

  it("test in local storage", () => {
    act(() => {
      saveLogs();
      dispatchEvent(new Event("storage"));
    });

    expect(JSON.parse(localStorage.getItem("logs") || "[]").length).toBe(2);
  });

  it("logs in container", async () => {
    expect((await screen.getAllByText(/[info]/i)).length).toBe(3);
  });

  it("clear terminal", () => {
    const clearButton = screen.getByText(/Очистить терминал/i);

    userEvent.click(clearButton);

    expect(JSON.parse(localStorage.getItem("logs") || "[]").length).toBe(1);
  });

  describe("log sended by client", () => {
    it("log sended to a server", async () => {
      const sendButton = screen.getByText(/Отправить/i);
      userEvent.click(sendButton);
      const toast = document.getElementsByClassName("toast")[0];
      const style = window.getComputedStyle(toast);
      expect(style.visibility).toBe("hidden");
    });
    it("toast shown", async () => {
      const toast = document.getElementsByClassName("toast")[0];
      const style = window.getComputedStyle(toast);
      expect(style.visibility).toBe("hidden");
    });
  });
});
