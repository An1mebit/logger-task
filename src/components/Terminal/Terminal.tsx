import { useEffect, useState } from "react";
import { ILogger } from "../../models/ILogger";
import "./terminal.scss";
import TerminalContainer from "./TerminalContainer/TerminalContainer";
import TerminalFilter from "./TerminalFilter/TerminalFilter";
import { IFilter } from "../../models/IFilter";
import dayjs from "dayjs";
import isBetween from "dayjs/plugin/isBetween";
import TerminalMessaging from "./TerminalMessaging/TerminalMessaging";
import Toast, { ToastColorScheme } from "../Toast/Toast";

dayjs.extend(isBetween);

export const Terminal = () => {
  const [logs, setLogs] = useState<ILogger[]>(
    JSON.parse(`${localStorage.getItem("logs")}` || "[]")
  );
  const [filter, setFilter] = useState<IFilter>({});
  const [filteredLogs, setFilteredLogs] = useState<ILogger[]>([]);
  const [isToast, setIsToast] = useState<boolean>(false);

  useEffect(() => {
    const checkLogsStorage = () => {
      setLogs(JSON.parse(localStorage.getItem("logs") || "[]"));
    };
    addEventListener("storage", checkLogsStorage);

    return () => removeEventListener("storage", checkLogsStorage);
  }, []);

  useEffect(() => {
    let filteredLogsNew = logs.filter((log) => log.level === filter.level);
    if (filter?.time) {
      console.log(
        dayjs(logs[0].timestamp).isBetween(
          filter?.time?.from || dayjs(),
          filter?.time?.to || dayjs(),
          "day",
          "[]"
        )
      );
      filteredLogsNew = filteredLogsNew.filter((log) =>
        dayjs(log.timestamp).isBetween(
          filter?.time?.from || dayjs(),
          filter?.time?.to || dayjs(),
          "day",
          "[]"
        )
      );
    }
    if (filteredLogsNew.length !== 0) {
      setFilteredLogs(filteredLogsNew);
    } else {
      setFilteredLogs([]);
    }
  }, [filter, logs]);

  const getUniqueStatus = (): string[] => {
    const statuses: string[] = [];
    logs.filter((item) => {
      if (
        statuses.findIndex((x) => x === item.level) === -1 &&
        (item?.level !== null || undefined)
      ) {
        statuses.push(item.level);
      }
    });

    return statuses;
  };

  return (
    <>
      <TerminalFilter
        statuses={getUniqueStatus()}
        filter={filter}
        setFilter={setFilter}
      />
      <TerminalContainer
        logs={filteredLogs.length === 0 ? logs : filteredLogs}
      />
      <TerminalMessaging setIsToast={setIsToast} />
      <Toast
        label="Успех"
        value="Сообщение успешно отправлено"
        color={ToastColorScheme.success}
        isVisible={isToast}
        setIsVisible={setIsToast}
      />
    </>
  );
};
