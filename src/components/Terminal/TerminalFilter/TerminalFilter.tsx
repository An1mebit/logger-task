import { Dispatch, FC, SetStateAction } from "react";
import { IFilter } from "../../../models/IFilter";

interface ITerminalFilterProps {
  statuses: string[];
  filter: IFilter;
  setFilter: Dispatch<SetStateAction<IFilter>>;
}

const TerminalFilter: FC<ITerminalFilterProps> = ({
  statuses,
  filter,
  setFilter,
}) => {
  const handlerChangeSelect = (value: string) => {
    setFilter({ ...filter, level: value });
    console.log(filter);
  };

  const handlerChangeTime = (key: "from" | "to", value: string) => {
    setFilter({
      ...filter,
      time: {
        ...filter.time,
        [key]: value,
      },
    });
    console.log(filter);
  };

  const handlerDropFiltersClick = () => {
    setFilter({});
  };

  return (
    <div style={{ display: "flex", gap: "10px", justifyContent: "center" }}>
      <select
        value={filter?.level || ""}
        onChange={(e) => handlerChangeSelect(e.target.value)}
      >
        <option value={""}></option>
        {statuses.map((status, index) => (
          <option key={index} value={status}>
            {status}
          </option>
        ))}
      </select>
      <input
        type="date"
        value={filter?.time?.from || ""}
        onChange={(e) => handlerChangeTime("from", e.target.value)}
      />
      <input
        type="date"
        value={filter?.time?.to || ""}
        onChange={(e) => handlerChangeTime("to", e.target.value)}
      />
      <button onClick={handlerDropFiltersClick}>Сбросить</button>
    </div>
  );
};

export default TerminalFilter;
