import { Dispatch, FC, SetStateAction, useEffect } from "react";
import "./toast.scss";

export enum ToastColorScheme {
  success = "#36D88A",
  error = "#EC0033",
}

interface IToastProps {
  value: string;
  label: string;
  color: ToastColorScheme;
  isVisible: boolean;
  setIsVisible: Dispatch<SetStateAction<boolean>>;
}

const Toast: FC<IToastProps> = ({
  value,
  label,
  color,
  isVisible,
  setIsVisible,
}) => {
  useEffect(() => {
    if (isVisible) {
      setTimeout(() => setIsVisible(false), 2000);
    }
  }, [isVisible, setIsVisible]);

  return (
    <div
      className="toast"
      style={{
        borderBottom: `8px solid ${color}`,
        opacity: isVisible ? "1" : "0",
        visibility: isVisible ? "visible" : "hidden",
      }}
    >
      <div className="toast_container">
        <p>{label}</p>
        <p>{value}</p>
      </div>
      <button>&times;</button>
    </div>
  );
};

export default Toast;
