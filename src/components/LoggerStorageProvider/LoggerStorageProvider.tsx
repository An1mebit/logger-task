import { createContext } from "react";

let socket = new WebSocket(import.meta.env.VITE_LOGGER_URL);

export const LoggerSocketContext = createContext(socket);

const LoggerStorageProvider = ({ children }: { children: JSX.Element }) => {
  if (socket !== null) {
    socket.addEventListener("error", () => {
      console.log("error");
    });
    socket.onmessage = (data) => {
      if (data !== null) {
        localStorage.setItem(
          "logs",
          JSON.stringify([
            ...JSON.parse(
              localStorage.getItem("logs") === null
                ? "[]"
                : localStorage.getItem("logs") || "[]"
            ),
            JSON.parse(data.data),
          ])
        );
        dispatchEvent(new Event("storage"));
      }
    };
  } else {
    socket = new WebSocket(import.meta.env.VITE_LOGGER_URL);
    socket.onerror = () => {
      console.log("error");
    };
  }
  return (
    <LoggerSocketContext.Provider value={socket}>
      {children}
    </LoggerSocketContext.Provider>
  );
};

export default LoggerStorageProvider;
