import { Link, useLocation } from "react-router-dom";
import "./header.scss";
import { CSSProperties } from "react";

const Header = () => {
  const location = useLocation();

  const hanadlerActiveLink = (path: string): CSSProperties => {
    if (path === location.pathname) {
      return {
        color: "#fff",
      };
    } else {
      return {
        color: "#ddd",
      };
    }
  };

  return (
    <header>
      <p className="title">{`[Тестовое]`}</p>
      <Link to={"/"} style={hanadlerActiveLink("/")}>
        Логгер
      </Link>
      <Link to={"/cam"} style={hanadlerActiveLink("/cam")}>
        Камера
      </Link>
    </header>
  );
};

export default Header;
