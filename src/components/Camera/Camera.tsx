import { useEffect, useRef } from "react";
import { loadPlayer } from "rtsp-relay/browser";

const Camera = () => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null);

  useEffect(() => {
    if (canvasRef.current !== null) {
      loadPlayer({
        url: "ws://localhost:2000/api/stream",
        canvas: canvasRef.current,
        onDisconnect: () => console.log("Connection lost!"),
        autoplay: true,
      });
    }
  }, [canvasRef]);

  return (
    <div style={{ width: 400, height: 300 }}>
      <canvas ref={canvasRef}></canvas>
    </div>
  );
};

export default Camera;
