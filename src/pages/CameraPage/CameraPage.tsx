import Camera from "../../components/Camera/Camera";

export const CameraPage = () => {
  return (
    <main>
      <Camera />
    </main>
  );
};
