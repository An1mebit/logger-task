import { Terminal } from "../../components/Terminal/Terminal";

export const LoggerPage = () => {
  return (
    <main>
      <Terminal />
    </main>
  );
};
