import { Routes, Route } from "react-router-dom";
import Header from "./components/Header/Header";
import { CameraPage, LoggerPage } from "./pages";
import "./App.scss";

function App() {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<LoggerPage />} />
        <Route path="/cam" element={<CameraPage />} />
      </Routes>
    </>
  );
}

export default App;
